import React from 'react';
import logo from './logo.svg';
import './App.css';
import CampaignForm from './components/CampaignForm';

function App() {
  return (
    <div className="App">
      <CampaignForm/>
    </div>
  );
}

export default App;
