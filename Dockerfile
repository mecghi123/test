# Stage 1: Build
FROM node:18-alpine AS build

# Tạo thư mục làm việc trong container
WORKDIR /app

# Copy file package.json và package-lock.json
COPY package*.json ./

# Cài đặt dependencies
RUN npm install

# Copy toàn bộ project vào container
COPY . .

# Build project
RUN npm run build

# Stage 2: Serve static files
FROM node:18-alpine

# Cài đặt `serve` để phục vụ file tĩnh
RUN npm install -g serve

# Copy thư mục build từ stage trước vào container
COPY --from=build /app/build /app/build

# Expose port 3000
EXPOSE 3000

# Chạy lệnh `serve` để chạy ứng dụng ở chế độ production
CMD ["serve", "-s", "app/build", "-l", "3000"]
